import { createApp } from 'vue'
import App from './App.vue'
import router from './router'



// iconos
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { faFontAwesome } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(fas, faFontAwesome)

createApp(App)
    .component('fa', FontAwesomeIcon)
    .use(router)
    .mount('#app')