from django.contrib import admin
from django.contrib.admin.sites import DefaultAdminSite
from django.contrib import admin
from django.db import router
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from BooksList import views as bviews
from TesisList import views as tviews
from users import views as uviews


router = DefaultRouter()
router.register(r'user', uviews.UsersViewSet, basename='user')
router.register(r'booklist', bviews.BooksViewSet, basename='booklist')
router.register(r'tesislist', tviews.TesisViewSet, basename='tesislist')

urlpatterns = [
    path('admin/', admin.site.urls),
    #Agregar path
    path('', include(router.urls)),
]
