from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets
from users.models import Profile
from users.serializers import UsersModelSerializer

class UsersViewSet (viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = UsersModelSerializer