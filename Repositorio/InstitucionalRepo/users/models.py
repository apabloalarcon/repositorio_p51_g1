from django.contrib.auth.models import User
from django.db import models

GENDER_CHOICES = ((0, 'Masculino'),(1, 'Femenino'),(2, 'Sin especificar'),)
ROL_CHOICES = ((0, 'Invitado'),(1, 'Profesor'), (2, 'Administrativo'), (3, 'Estudiante Posgrado'), (4, 'Estudiante Pregrado'))

class Profile(models.Model):
    #Modelo del perfil

    FACULTY_CHOICES = ((0, 'N/A'),(1, 'Ciencia y Tecnología'), (2, 'Bellas Artes'),
    (3, 'Educación'), (4, 'Ciencias Sociales'), (5, 'Ingeniería'))

    #Usuario es único
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    website = models.URLField(max_length=200, blank=True)
    discription = models.TextField(blank=True)
    faculty = models.IntegerField(choices=FACULTY_CHOICES, null=0)
    rol = models.IntegerField(choices=ROL_CHOICES, null=True)
    phone_number = models.CharField(max_length=10, null=True) 
    recovery_email = models.EmailField(max_length=50)

    #Profile picture 👀
    profile_picture = models.ImageField(upload_to='users/pictures', blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    gender = models.IntegerField(choices=GENDER_CHOICES)

# Create your models here.
