from django.apps import AppConfig

#User app configuración
class UsersConfig(AppConfig):
    
    name = 'users'
    verbose_name = 'Users'
