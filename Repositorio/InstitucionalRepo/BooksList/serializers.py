from rest_framework import serializers
from BooksList.models import InputBooks

class BooksModelSerializer (serializers.ModelSerializer):
    class Meta:
        model = InputBooks
        fields = '__all__'