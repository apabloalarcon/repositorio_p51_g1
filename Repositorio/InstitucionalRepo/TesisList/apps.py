from django.apps import AppConfig


class TesislistConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'TesisList'
