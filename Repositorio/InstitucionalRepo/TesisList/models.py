from django.db import models

FACULTY_CHOICES = ((0, 'N/A'),(1, 'Ciencia y Tecnología'), (2, 'Bellas Artes'),
(3, 'Educación'), (4, 'Ciencias Sociales'), (5, 'Ingeniería'))
PROGRAM_CHOICES = ((0, 'Matemáticas'),(1, 'Ciencias Sociales'), (2, 'Música'),
(3, 'Pedagogía'), (4, 'Ingeniería Física'), (5, 'Deportes'))

class InputTesis(models.Model):
    title = models.CharField(max_length=100)
    faculty = models.IntegerField(choices=FACULTY_CHOICES, blank=True)
    program = models.IntegerField(choices = PROGRAM_CHOICES,blank=True)
    abstract = models.TextField(blank=True)

    date = models.DateTimeField()
    author_or_authors = models.CharField(max_length=250,blank=True)
    advisor_or_supervisor = models.CharField(max_length=250,blank=True)
    key_words = models.CharField(max_length=100, blank=True)
    URL = models.URLField(max_length=200, blank=True)
    file = models.FileField(upload_to='files/', blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
