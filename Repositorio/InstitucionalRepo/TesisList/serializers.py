from rest_framework import serializers
from TesisList.models import InputTesis

class TesisModelSerializer (serializers.ModelSerializer):
    class Meta:
        model = InputTesis
        fields = '__all__'